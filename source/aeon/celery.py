import os

from celery import Celery
from celery.schedules import crontab

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'aeon.settings')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app = Celery('aeon')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
app.conf.beat_schedule = {
    'say_hello': {
        'task': 'hello.tasks.say_hello',
        'schedule': crontab(minute='*'),
    },
}


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
