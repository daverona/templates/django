# daverona/django

[![pipeline status](https://gitlab.com/daverona/templates/django/badges/master/pipeline.svg)](https://gitlab.com/daverona/templates/django/-/commits/master)
[![coverage report](https://gitlab.com/daverona/templates/django/badges/master/coverage.svg)](https://gitlab.com/daverona/templates/django/-/commits/master)

This is a repository for Docker images of Django application.

* GitLab source repository: [https://gitlab.com/daverona/templates/django](https://gitlab.com/daverona/templates/django)
* GitLab container registry: [https://gitlab.com/daverona/templates/django/container\_registry/](https://gitlab.com/daverona/templates/django/container_registry/)

## Prerequisites

* [Docker](https://docs.docker.com/get-docker/) is installed.
* [Traefik](https://docs.traefik.io/) is installed. ([This](http://gitlab.com/daverona/docker-compose/traefik) is recommended.)
* `aeon.example` needs to map to 127.0.0.1 for development. ([dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) is recommended.)

## Quick Start

### Development

To test-drive the development version:

```bash
cp docker-compose.development.yml docker-compose.yml
cp .env.development .env
docker-compose build
docker-compose run --rm app python3 manage.py migrate
docker-compose down
docker-compose up -d
```

Then visit [http://aeon.example/](http://aeon.example/).
If "The install worked successfully! Congratulations!" page shows up, it's a success.

(Optional) To skip the image building, do the following instead of `docker-compose build`:

```bash
docker image pull registry.gitlab.com/daverona/templates/django:draft
docker image tag registry.gitlab.com/daverona/templates/django:draft daverona/django:draft
```

### Production

To test-drive the production version:

```bash
cp docker-compose.production.yml docker-compose.yml
cp .env.production .env
docker-compose build
docker-compose run --rm gunicorn python3 manage.py migrate
docker-compose run --rm gunicorn python3 manage.py collectstatic --no-input
docker-compose down
docker-compose up -d
```

Then visit [http://aeon.example/admin/](http://aeon.example/admin/).
If "Django administration" page shows up, it's a success.

(Optional) To skip the image building, do the following in place of `docker-compose build`:

```bash
docker image pull registry.gitlab.com/daverona/templates/django:final
docker image tag registry.gitlab.com/daverona/templates/django:final daverona/django:final
```

## Bootstrapping

To start a project with a clean slate:

```bash
# Stop container
docker-compose down

# Empty source
rm -rf source

# Create a project (specify Django version if you will)
docker container run --rm \
  --volume $PWD/source:/source \
  python:3.7-alpine3.10 \
  ash -c "\
    pip install 'Django>=3' 'celery>=4' 'django-redis>=4' \
    && django-admin startproject aeon /source \
    && pip freeze > /source/requirements.txt \
    && echo -e \"-r requirements.txt\n\n# Packages for development only\" > /source/requirements.development.txt \
    && echo -e \"-r requirements.txt\n\n# Packages for production only\" > /source/requirements.production.txt \
    && wget -q -O- https://raw.githubusercontent.com/github/gitignore/master/Python.gitignore > /source/.gitignore \
    && wget -q -O- https://raw.githubusercontent.com/github/gitignore/master/Global/VirtualEnv.gitignore >> /source/.gitignore \
    && wget -q -O- https://raw.githubusercontent.com/django/django/master/.gitignore >> /source/.gitignore \
    && echo \"db.sqlite3\" >> /source/.gitignore \
    && chown -R $(id -u):$(id -g) /source"
```

Update `source/aeon/settings.py` so that it contains:

```python
APP_ENV = os.getenv('APP_ENV', 'production').strip().lower()
APP_DOMAIN = os.getenv('APP_DOMAIN', 'aeon.example').strip().lower()
# @see https://docs.djangoproject.com/en/3.0/ref/settings/#debug
DEBUG = APP_ENV != 'production'
# @see https://docs.djangoproject.com/en/3.0/ref/settings/#allowed-hosts
ALLOWED_HOSTS = [APP_DOMAIN, ] if not DEBUG else ['localhost', '127.0.0.1', '[::1]', APP_DOMAIN, ]
# @see https://docs.djangoproject.com/en/3.0/ref/settings/#static-root
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
```

`source` contains a brand-new project.  To run, do "Quick Start" section.

## Why Should I Use This?

1. *Fast*: Dockerfiles take as less time as possible to build images once `source` changes.
2. *Small*: Dockerfiles contain no build-time dependencies.
3. *Clean*: `source` directory can serve as project root *without* Docker.
4. *Complete*: Dockerfiles contain exact services required in common projects for each mode.

### Installed Services

#### Development

* Django app server (`python3 manage.py runserver`)
* [Celery](https://docs.celeryproject.org/en/latest/) server
* Redis server
* Supervisor (process control)

#### Production

* Gunicorn server
* Nginx server
* [Celery](https://docs.celeryproject.org/en/latest/) server
* Logrotate service
* Redis server
* Supervisor (process control)

## Cloning

Time to make this template yours:

```bash
git clone https://gitlab.com/daverona/templates/django.git
cd django
rm -rf .git
git init
git add .
git commit -m "Clone daverona/django"
```

You have done for a local repository. If you have set up an empty remote repository, say `git@gitserver.com:django.git`, set and push to remote:

```bash
git remote add origin git@gitserver.com:django.git
git push --set-upstream origin master
```

### Directory Structure

```
docker/                   ; Docker directory                
  draft/                    ; deveopment version
    Dockerfile
    docker-entrypoint.sh
    supervisor/
      app.ini                 ; Django app server
      celery.ini              ; Celery server
      redis.ini               ; Redis server
      supervisor.ini          ; Supervisor
  final/                    ; production version
    Dockerfile
    docker-entrypoint.sh
    logrotate/                ; Logrotate configuration
      ...
    nginx/                    ; Nginx configuration
      ...
    supervisor/
      celery.ini              ; Celery server
      clond.ini               ; Logrotate service (by crond)
      gunicorn.ini            ; Gunicorn server
      nginx.ini               ; Nginx server
      redis.ini               ; Redis server
      supervisor.ini          ; Supervisor
source/                   ; Django project root
  ...
```

## Notes

* `APP_ENV` is hard-coded in Docker images and does not need to be specified in `.env` file.

### Development

* If `source/requirements.txt` and/or `source/requirements.development.txt` are updated, build your development Docker image again.

### Production

* You should *never* write down any secrets in `source/aeon/settings.py` for production;
they will be stored and distributed with Docker images.  Instead specify key-value pairs in `.env` 
and read them as environment variables from `source/aeon/settings.py` as shown in "Bootstapping" section.
* The stock `SECRET_KEY` value should not be used. To generate a new `SECRET_KEY` value:

```bash
docker container run --rm \                                                                                                                                                                                                                                                                                                                 20:13:48
  daverona/django:final \
  python3 -c "from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())"
```

* Before deploy your Django app, read these:
  * [Deployment checklist](https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/)
  * [Error reporting](https://docs.djangoproject.com/en/3.0/howto/error-reporting/)
* Distribute the following only:
  * `.env.final`
  * `docker-compose.final.yml`
  * production Docker image

## Advertisement

This is a part of my *Web Framework in Docker* series. Please check out:

* daverona/django: [https://gitlab.com/daverona/templates/django](https://gitlab.com/daverona/templates/django) &mdash; Python
* daverona/laravel: [https://gitlab.com/daverona/templates/laravel](https://gitlab.com/daverona/templates/laravel) &mdash; PHP
* daverona/rails: [https://gitlab.com/daverona/templates/rails](https://gitlab.com/daverona/templates/rails) &mdash; Ruby
* daverona/vue: [https://gitlab.com/daverona/templates/vue](https://gitlab.com/daverona/templates/vue) &mdash; JavaScript

## References

* [https://docs.djangoproject.com/en/3.0/](https://docs.djangoproject.com/en/3.0/)

<!-- I wonder, should I need to keep gettext in runtime or not. -->
